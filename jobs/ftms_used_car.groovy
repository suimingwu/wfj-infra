pipelineJob('ftms-used-car') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-used-car.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-used-car.jenkinsfile'))
            sandbox()
        }
    }
}