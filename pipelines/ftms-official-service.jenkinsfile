@Library(('ftms-official-platform-jenkins-libs')) _
pipeline {
    agent any
    environment {
        DOCKER_REGISTRY = "192.168.119.4:5000"
        PROD_DOCKER_REGISTRY = "registry.ftms-k8s.com.cn/ftms-gfxt"
    }

    stages {
        stage("⚡ Checkout code") {
            steps {
                script {
                    pipeCheckScm("git", "*/master")
                    env.GITCOMMIT_ID = sh(returnStdout: true, script: 'git rev-parse --short HEAD').trim()
                    env.GIT_BRANCH = sh(returnStdout: true, script: 'git name-rev --name-only HEAD').trim()
                }
            }
        }

        stage("✅ Test package") {
            steps {
                script {
                    pipeTest()
                }
            }
        }

        stage("⛓ Build image and push") {
            steps {
                script {
                    def dockerfile = dockerfile()
                    writeFile file: 'Dockerfile', text: dockerfile
                    docker.withRegistry("http://${DOCKER_REGISTRY}") {
                        def image = docker.build("ftms-official-service:${GITCOMMIT_ID}")
                        image.push()
                        image.push("latest")
                    }
                }
            }
        }

        stage("⛩ Deploy app to dev") {
            steps{
                script {
                    env.DEPLOY_ENV = "dev"
                    pipeDeploy()
                }
            }
        }

        stage("⏳ Approval of deploy to testing") {

            steps {
                script {
                    try {
                        env.DEPLOY_TEST_FLAG = "true"
                        timeout(time: 5, unit: "MINUTES") {
                            input message: "Deploy ftms-official-service to test?"
                        }
                    } catch(err) {
                        env.DEPLOY_TEST_FLAG = "false"
                    }
                }
            }
        }

        stage("⛩ Deploy app to testing") {
            when {
                expression { DEPLOY_TEST_FLAG == "true" }
            }
            steps{
                script {
                    env.DEPLOY_ENV = "testing"
                    pipeDeploy()
                }
            }
        }

        stage("⏳ Approval of deploy to uat") {
            steps {
                script {
                    try {
                        env.DEPLOY_UAT_FLAG = "true"
                        timeout(time: 5, unit: "MINUTES") {
                            input message: "Deploy ftms-official-service to uat?"
                        }
                    } catch(err) {
                        env.DEPLOY_UAT_FLAG = "false"
                    }
                }
            }
        }

        stage("⛩ Deploy app to uat") {
            when {
                expression { DEPLOY_UAT_FLAG == "true" }
            }
            steps{
                script {
                    env.DEPLOY_ENV = "uat"
                    pipeDeploy()
                }
            }
        }
    }
    post {
        failure {
            script {
                notify.weComNotify("构建失败 ❌")
            }
        }
    }

}

def dockerfile() {
    return '''
FROM 192.168.119.4:5000/gradle:6.6.1-jdk11 as builder

COPY . /data/ftms
WORKDIR /data/ftms

RUN gradle build -x test --no-build-cache

FROM 192.168.119.4:5000/adoptopenjdk/openjdk11:alpine-jre

RUN apk add --no-cache tzdata
ENV TZ=Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

COPY --from=builder /data/ftms/build/libs/*.jar app.jar

RUN sh -c 'touch /app.jar'

ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -jar /app.jar" ]

EXPOSE 8080

    '''
}