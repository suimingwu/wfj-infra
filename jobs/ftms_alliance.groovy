pipelineJob('ftms-alliance') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-alliance.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-alliance.jenkinsfile'))
            sandbox()
        }
    }
}