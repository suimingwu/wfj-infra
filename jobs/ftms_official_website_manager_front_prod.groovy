pipelineJob('ftms-official-website-manager-front-prod') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-official-website-manager-front.git')
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-official-website-manager-front-prod.jenkinsfile'))
            sandbox()
        }
    }
}