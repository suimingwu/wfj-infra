pipelineJob('ftms-official-website-manager-site-prod') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-official-website-manager-site.git')
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-official-website-manager-site-prod.jenkinsfile'))
            sandbox()
        }
    }
}