pipelineJob('ftms-website-h5') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-website-h5.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-website-h5.jenkinsfile'))
            sandbox()
        }
    }
}