pipelineJob('ftms-website-h5-prod') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-website-h5.git')
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-website-h5-prod.jenkinsfile'))
            sandbox()
        }
    }
}