pipelineJob('ftms-website-service') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms_website.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-website-service.jenkinsfile'))
            sandbox()
        }
    }
}