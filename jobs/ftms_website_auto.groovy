pipelineJob('ftms-website-auto') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-website-auto.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-website-auto.jenkinsfile'))
            sandbox()
        }
    }
}