pipelineJob('ftms-official-platform-website-prod') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-official-platform-website.git')
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-official-platform-website-prod.jenkinsfile'))
            sandbox()
        }
    }
}