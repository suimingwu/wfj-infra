pipelineJob('ftms-union-clues-prod') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-union-clues.git')
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-union-clues-prod.jenkinsfile'))
            sandbox()
        }
    }
}