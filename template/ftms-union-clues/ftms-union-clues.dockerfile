FROM 192.168.119.4:5000/php:5.6.22-fpm

EXPOSE 80

COPY . /app/ftms-union-clues/
WORKDIR /app/ftms-union-clues/
RUN ls /app/ftms-union-clues/|grep -v Config|xargs chmod 777 -R
RUN    apt update \
    && apt install -y libfreetype6-dev libjpeg62-turbo-dev libpng-dev nginx \
    && docker-php-ext-install -j$(nproc) iconv \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-configure pdo_mysql \
    && docker-php-ext-install pdo_mysql \
    && pecl install redis-4.3.0 \
    && docker-php-ext-enable redis


RUN    echo "server {" >>/etc/nginx/conf.d/ftms.conf \
    && echo "    listen  80;" >>/etc/nginx/conf.d/ftms.conf \
    && echo "    server_name *.ftms.com.cn ftms-union-clues-svc;" >>/etc/nginx/conf.d/ftms.conf \
    && echo "    root    /app/ftms-union-clues;" >>/etc/nginx/conf.d/ftms.conf \
    && echo "    location / {" >>/etc/nginx/conf.d/ftms.conf \
    && echo "        index  index.php toyota.php;" >>/etc/nginx/conf.d/ftms.conf \
    && echo "        if (!-e \\$request_filename) {" >>/etc/nginx/conf.d/ftms.conf \
    && echo "            rewrite  ^(.*)\\$  /index.php?s=\\$1  last;" >>/etc/nginx/conf.d/ftms.conf \
    && echo "            break;" >>/etc/nginx/conf.d/ftms.conf \
    && echo "        }" >>/etc/nginx/conf.d/ftms.conf \
    && echo "    }" >>/etc/nginx/conf.d/ftms.conf \
    && echo "    location ~ \\.php$ {" >>/etc/nginx/conf.d/ftms.conf \
    && echo "        fastcgi_pass   127.0.0.1:9000;" >>/etc/nginx/conf.d/ftms.conf \
    && echo "        fastcgi_index  index.php;" >>/etc/nginx/conf.d/ftms.conf \
    && echo "        fastcgi_param  SCRIPT_FILENAME  \\$document_root\\$fastcgi_script_name;" >>/etc/nginx/conf.d/ftms.conf \
    && echo "        include        fastcgi_params;" >>/etc/nginx/conf.d/ftms.conf \
    && echo "    }" >>/etc/nginx/conf.d/ftms.conf \
    && echo "}" >>/etc/nginx/conf.d/ftms.conf

CMD ["/bin/sh","-c","nginx -c /etc/nginx/nginx.conf && nginx -s reload && php-fpm"]
