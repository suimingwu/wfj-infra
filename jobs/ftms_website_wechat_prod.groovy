pipelineJob('ftms-website-wechat-prod') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-website-wechat.git')
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-website-wechat-prod.jenkinsfile'))
            sandbox()
        }
    }
}