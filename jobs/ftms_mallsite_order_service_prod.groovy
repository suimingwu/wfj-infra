pipelineJob('ftms-mallsite-order-service-prod') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-mallsite-order-service.git')
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-mallsite-order-service-prod.jenkinsfile'))
            sandbox()
        }
    }
}