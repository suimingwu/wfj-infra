package vars

def call(dockerfileName) {
    def dockerfile = libraryResource "Dockerfile/${dockerfileName}.dockerfile"
    writeFile file: 'Dockerfile', text: dockerfile

    docker.withRegistry("http://${DOCKER_REGISTRY}") {
        def image = docker.build("${dockerfileName}:${GITCOMMIT_ID}")
        image.push()
        image.push("latest")
    }
}