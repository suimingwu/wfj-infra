def call(cardid) {
    sshagent(['92a1a45e-4397-4664-8095-50f9931d5ddc']) {
        sh("""
            #!/usr/bin/env bash
            timestamp=\$(date +%Y%m%d%H%M%S)
            git tag release_\${timestamp}_${cardid}
            git push origin release_\${timestamp}_${cardid}
         """)
    }
}