pipelineJob('ftms-website-pc') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-website-pc.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-website-pc.jenkinsfile'))
            sandbox()
        }
    }
}