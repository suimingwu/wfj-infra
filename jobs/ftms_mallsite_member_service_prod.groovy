pipelineJob('ftms-mallsite-member-service-prod') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-mallsite-member-service.git')
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-mallsite-member-service-prod.jenkinsfile'))
            sandbox()
        }
    }
}