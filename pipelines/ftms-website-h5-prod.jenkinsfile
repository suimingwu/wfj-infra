@Library(('ftms-official-platform-jenkins-libs')) _
pipeline {
    agent any
    parameters{
        string(name: 'VERSION', defaultValue: 'master', description: '填写部署的版本，可以是分支也可以是commit ID')
        string(name: 'CardID', defaultValue: 'NOP-562', description: '填写版本发布卡号')
    }
    environment {
        PROD_DOCKER_REGISTRY = "registry.ftms-k8s.com.cn/ftms-gfxt"
    }

    stages {
        stage("⚡ Checkout code") {
            steps {
                script {
                    pipeCheckScm("git@github.com:tw-ftms-official-platform/ftms-website-h5.git", "${params.VERSION}")
                    env.GITCOMMIT_ID = sh(returnStdout: true, script: 'git rev-parse --short HEAD').trim()
                    env.GIT_BRANCH = sh(returnStdout: true, script: 'git name-rev --name-only HEAD').trim()
                }
            }
        }
        stage("⚡ Check image exists") {
            steps {
                script {
                    env.IMAGE_EXISTS = "false"
                    if (checkImageExists("ftms-website-h5","${GITCOMMIT_ID}")) {
                        env.DOCKER_REGISTRY = "192.168.119.4:5000"
                        env.JOB_BASE_NAME = "ftms-website-h5"
                        pipePushProdImage()
                        env.IMAGE_EXISTS = "true"
                    }
                }
            }
        }
        stage("⛩ push app image to prod registry") {
            when {
                expression { IMAGE_EXISTS == "false" }
            }
            steps{
                script {
                    def dockerfile = dockerfile()
                    writeFile file: 'Dockerfile', text: dockerfile
                    docker.withRegistry("https://${PROD_DOCKER_REGISTRY}", "prod-k8s-registry") {
                        def image = docker.build("ftms-gfxt/ftms-website-h5:${GITCOMMIT_ID}")
                        image.push()
                        image.push("latest")
                    }
                }
            }
        }
        stage("⏳ Approval of deploy to prod") {
            steps {
                script {
                    try {
                        env.DEPLOY_PROD_FLAG = "true"
                        timeout(time: 5, unit: "MINUTES") {
                            input message: "Deploy ftms-website-h5 to prod?"
                        }
                    } catch(err) {
                        env.DEPLOY_PROD_FLAG = "false"
                    }
                }
            }
        }
        stage("⛩ Deploy app to prod") {
            when {
                expression { DEPLOY_PROD_FLAG == "true" }
            }
            steps{
                script {
                    env.DEPLOY_ENV = "prod"
                    env.JOB_BASE_NAME = "ftms-website-h5"
                    pipeDeploy()
                }
            }
        }
        stage("✅ set release Tag to github") {
            when {
                expression { DEPLOY_PROD_FLAG == "true" }
            }
            steps{
                script {
                    setTag("${params.CardID}")
                }
            }
        }
    }
    post {
        failure {
            script {
                notify.weComNotify("构建失败 ❌")
            }
        }
    }
}

def dockerfile() {
    return '''
FROM 192.168.119.4:5000/node:14.19-alpine  as builder

COPY ./ /root/project

WORKDIR /root/project

RUN npm install --registry http://114.113.80.184:8003/repository/npm-proxy/ \
	&& npm run build

FROM 192.168.119.4:5000/nginx:1.20.2-alpine

COPY --from=builder /root/project/build /usr/share/nginx/html
COPY --from=builder /root/project/build /usr/share/nginx/html/wap-ft
COPY --from=builder /root/project/nginx.conf.template /etc/nginx/conf.d

EXPOSE 80

CMD cat /etc/nginx/conf.d/nginx.conf.template > /etc/nginx/conf.d/default.conf  \
	&& nginx -g 'daemon off;'

    '''
}