pipelineJob('ec-admin') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ec-admin.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ec-admin.jenkinsfile'))
            sandbox()
        }
    }
}