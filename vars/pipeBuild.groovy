package vars

def call() {
    def runArgs = " --privileged -u root -v /data/dind-gradle-cache/dind-cache/image:/var/lib/docker/image -v /data/dind-gradle-cache/dind-cache/overlay2:/var/lib/docker/overlay2"

    docker.image("gradle:6.6.1-jdk11").inside(runArgs) {
            sh "nohup /usr/local/bin/dockerd-entrypoint.sh &"
            def shellString = /s#http.*#file\:/
            sh "sed -i ${shellString}///opt/gradle/gradle.6.6.1.zip# ./gradle/wrapper/gradle-wrapper.properties"
            sh "./gradlew build -x test --no-build-cache"
    }
}
