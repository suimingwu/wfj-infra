pipelineJob('ftms-website-e2e') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-website-e2e.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-website-e2e.jenkinsfile'))
            sandbox()
        }
    }
}