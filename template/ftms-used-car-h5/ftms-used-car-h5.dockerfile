FROM keymetrics/pm2:14-alpine

COPY ./ /root/project

WORKDIR /root/project

RUN apk update && apk add git
RUN git config --global url."https://".insteadOf git://

RUN npm install

RUN npm run build

CMD [ "sh", "-c", "pm2-runtime start $DEPLOY_ENV " ]

EXPOSE 20898
