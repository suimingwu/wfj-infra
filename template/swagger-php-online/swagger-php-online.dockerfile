FROM php:8.1-fpm-buster

EXPOSE 8000

COPY . /swagger-php

RUN apt-get update && apt-get install -y \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libpng-dev \
    git \
    wget \
    && docker-php-ext-configure gd --with-freetype --with-jpeg \
    && docker-php-ext-install -j$(nproc) gd

RUN mkdir /root/.ssh/
ADD id_rsa /root/.ssh/id_rsa
RUN touch /root/.ssh/known_hosts \
    && ssh-keyscan github.com>> /root/.ssh/known_hosts \
    && chmod 0600 /root/.ssh/id_rsa
RUN git clone -b master git@github.com:tw-ftms-official-platform/ftms-union.git
RUN wget https://raw.githubusercontent.com/composer/getcomposer.org/76a7060ccb93902cd7576b67264ad91c8a2700e2/web/installer -O - -q | php -- --quiet \
    && mv composer.phar /usr/local/bin/composer \
    && composer config --global --auth github-oauth.github.com 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
RUN cd /swagger-php \
    && composer global require zircote/swagger-php
CMD ["/bin/sh","-c","/root/.composer/vendor/bin/openapi ./ -l -o /swagger-php/openapi.json | php -S 0.0.0.0:8000 /swagger-php/ftmsSwaggerService.php"]