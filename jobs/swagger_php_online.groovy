pipelineJob('swagger-php-online') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/swagger-php-online.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/swagger-php-online.jenkinsfile'))
            sandbox()
        }
    }
}