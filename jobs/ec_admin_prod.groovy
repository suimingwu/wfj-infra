pipelineJob('ec-admin-prod') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ec-admin.git')
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ec-admin-prod.jenkinsfile'))
            sandbox()
        }
    }
}