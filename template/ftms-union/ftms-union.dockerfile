FROM 192.168.119.4:5000/php:5.6.40-fpm

EXPOSE 80

RUN ln -snf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && echo Asia/Shanghai > /etc/timezone
RUN    apt update \
    && apt install -y libfreetype6-dev libjpeg62-turbo-dev libpng-dev nginx \
    && docker-php-ext-install -j$(nproc) iconv \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-configure pdo_mysql \
    && docker-php-ext-install pdo_mysql \
    && pecl install redis-4.3.0 \
    && docker-php-ext-enable redis
COPY . /app/ftms-union/
WORKDIR /app/ftms-union/
RUN ls /app/ftms-union/|grep -v Config|xargs chmod 777 -R
CMD ["/bin/sh","-c","nginx -c /etc/nginx/nginx.conf && nginx -s reload && php-fpm"]
