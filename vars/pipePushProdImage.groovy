package vars

def call() {
    docker.withRegistry("http://${DOCKER_REGISTRY}") {
        def image = docker.image("${DOCKER_REGISTRY}/${JOB_BASE_NAME}:${GITCOMMIT_ID}")
        image.pull()
    }
    sh "docker tag ${DOCKER_REGISTRY}/${JOB_BASE_NAME}:${GITCOMMIT_ID} ${PROD_DOCKER_REGISTRY}/${JOB_BASE_NAME}:${GITCOMMIT_ID}"
    docker.withRegistry("https://${PROD_DOCKER_REGISTRY}", "prod-k8s-registry") {
        def image = docker.image("${PROD_DOCKER_REGISTRY}/${JOB_BASE_NAME}:${GITCOMMIT_ID}")
        image.push()
        image.push("latest")
    }
}