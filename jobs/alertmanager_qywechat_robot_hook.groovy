pipelineJob('alertmanager-qywechat-robot-hook') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/alertmanager-qywechat-robot-hook.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/alertmanager-qywechat-robot-hook.jenkinsfile'))
            sandbox()
        }
    }
}