pipelineJob('ftms-mallsite-app-prod') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-mallsite-app.git')
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-mallsite-app-prod.jenkinsfile'))
            sandbox()
        }
    }
}