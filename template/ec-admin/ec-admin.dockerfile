FROM 192.168.119.4:5000/maven:3.3.9-jdk-8 as builder
COPY . /data/ftms
WORKDIR /data/ftms
RUN mvn clean package -U -Dmaven.test.skip=true -Ptest

FROM 192.168.119.4:5000/tomcat:9.0.59-jdk8-temurin-focal

ENV TZ=Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

COPY --from=builder /data/ftms/ec-admin-order/target/ec-order.war /usr/local/tomcat/webapps/
COPY --from=builder /data/ftms/ec-admin-member/target/ec-member.war /usr/local/tomcat/webapps/
COPY --from=builder /data/ftms/ec-admin-product/target/ec-ccgoods.war /usr/local/tomcat/webapps/
COPY --from=builder /data/ftms/ec-admin-promote/target/ec-promote.war /usr/local/tomcat/webapps/
COPY --from=builder /data/ftms/ec-admin-system/target/ec-system.war /usr/local/tomcat/webapps/

EXPOSE 8080