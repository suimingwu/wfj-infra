FROM 192.168.119.4:5000/node:14.19-alpine  as builder

COPY ./ /root/project

WORKDIR /root/project

RUN npm install --registry http://114.113.80.184:8003/repository/npm-proxy/ \
	&& npm run build

FROM 192.168.119.4:5000/nginx:1.20.2-alpine

COPY --from=builder /root/project/build /usr/share/nginx/html
COPY --from=builder /root/project/build /usr/share/nginx/html/wap-ft
COPY --from=builder /root/project/nginx.conf.template /etc/nginx/conf.d

EXPOSE 80

CMD cat /etc/nginx/conf.d/nginx.conf.template > /etc/nginx/conf.d/default.conf  \
	&& nginx -g 'daemon off;'
