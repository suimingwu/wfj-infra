pipelineJob('ftms-wap-prod') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-wap.git')
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-wap-prod.jenkinsfile'))
            sandbox()
        }
    }
}