@Library(('ftms-official-platform-jenkins-libs')) _
pipeline {
    agent any
    parameters{
        string(name: 'VERSION', defaultValue: 'master', description: '填写部署的版本，可以是分支也可以是commit ID')
        string(name: 'CardID', defaultValue: 'NOP-562', description: '填写版本发布卡号')
    }
    environment {
        PROD_DOCKER_REGISTRY = "registry.ftms-k8s.com.cn/ftms-gfxt"
    }

    stages {
        stage("⚡ Checkout code") {
            steps {
                script {
                    pipeCheckScm("git@github.com:tw-ftms-official-platform/${jobName}.git", "\${params.VERSION}")
                    env.GITCOMMIT_ID = sh(returnStdout: true, script: 'git rev-parse --short HEAD').trim()
                    env.GIT_BRANCH = sh(returnStdout: true, script: 'git name-rev --name-only HEAD').trim()
                }
            }
        }
        stage("⚡ Check image exists") {
            steps {
                script {
                    env.IMAGE_EXISTS = "false"
                    if (checkImageExists("${jobName}","\${GITCOMMIT_ID}")) {
                        env.DOCKER_REGISTRY = "192.168.119.4:5000"
                        env.JOB_BASE_NAME = "${jobName}"
                        pipePushProdImage()
                        env.IMAGE_EXISTS = "true"
                    }
                }
            }
        }
        stage("⛩ push app image to prod registry") {
            when {
                expression { IMAGE_EXISTS == "false" }
            }
            steps{
                script {
                    def dockerfile = dockerfile()
                    writeFile file: 'Dockerfile', text: dockerfile
                    docker.withRegistry("https://\${PROD_DOCKER_REGISTRY}", "prod-k8s-registry") {
                        def image = docker.build("ftms-gfxt/${jobName}:\${GITCOMMIT_ID}")
                        image.push()
                        image.push("latest")
                    }
                }
            }
        }
        stage("⏳ Approval of deploy to prod") {
            steps {
                script {
                    try {
                        env.DEPLOY_PROD_FLAG = "true"
                        timeout(time: 5, unit: "MINUTES") {
                            input message: "Deploy ${jobName} to prod?"
                        }
                    } catch(err) {
                        env.DEPLOY_PROD_FLAG = "false"
                    }
                }
            }
        }
        stage("⛩ Deploy app to prod") {
            when {
                expression { DEPLOY_PROD_FLAG == "true" }
            }
            steps{
                script {
                    env.DEPLOY_ENV = "prod"
                    env.JOB_BASE_NAME = "${jobName}"
                    env.JOB_BASE_NAME = "${jobName}"
                    pipeDeploy()
                }
            }
        }
        stage("✅ set release Tag to github") {
            when {
                expression { DEPLOY_PROD_FLAG == "true" }
            }
            steps{
                script {
                    setTag("\${params.CardID}")
                }
            }
        }
    }
    post {
        failure {
            script {
                notify.weComNotify("构建失败 ❌")
            }
        }
    }
}

def dockerfile() {
    return '''
${dockerfilecontent}
    '''
}