# product-nginx 

用于保存生产nginx配置

## www.ftms.com.cn

官网配置,主要包含以下域名:
- www.ftms.com.cn 官网PC
- m.ftms.com.cn 官网WAP
- wx-m.ftms.com.cn 官微

nginx部署在192.168.101.96和192.168.101.97机器上,使用keepalived绑定了虚拟IP 192.168.101.40
公网域名解析的IP为114.113.80.227，映射到内网虚拟IP 192.168.101.40

## mall.ftms.com.cn

商城配置,主要包含以下域名:
- mall.ftms.com.cn 商城PC
- mall-admin.ftms.com.cn 商城后台

nginx部署在192.168.101.64和192.168.101.65机器上,使用keepalived绑定了虚拟IP 192.168.101.86
公网域名解析的IP为114.113.80.229，映射到内网虚拟IP 192.168.101.86

## platform.ftms.com.cn

智能运营平台配置,主要包含以下域名:
- platform.ftms.com.cn 智能运营平台
- api.ftms.com.cn 
- app.ftms.com.cn 

nginx部署在192.168.101.27机器上
公网域名解析的IP为114.113.80.251，映射到内网IP 192.168.101.27