package vars

def call(gitUrl, branch) {
    checkout scm: [$class: 'GitSCM', branches: [[name: branch]], 
    doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], 
    userRemoteConfigs: [[credentialsId: '92a1a45e-4397-4664-8095-50f9931d5ddc', url: gitUrl]]]
}