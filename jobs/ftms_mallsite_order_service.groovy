pipelineJob('ftms-mallsite-order-service') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-order.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-mallsite-order-service.jenkinsfile'))
            sandbox()
        }
    }
}