FROM 192.168.119.4:5000/maven:3.8.5-openjdk-8 as builder

COPY . /data/ftms
COPY ./src/main/resources/images /opt/UsedCarImg
WORKDIR /data/ftms

RUN unset MAVEN_CONFIG && mvn clean package -U -Dmaven.test.skip=true

FROM 192.168.119.4:5000/openjdk:8-jdk

ENV TZ=Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

COPY --from=builder /data/ftms/target/*.jar app.jar
COPY --from=builder /opt/UsedCarImg /opt/UsedCarImg

RUN sh -c 'touch /app.jar'

CMD [ "sh", "-c", "java $JAVA_OPTS -jar  /app.jar" ]

EXPOSE 8081
