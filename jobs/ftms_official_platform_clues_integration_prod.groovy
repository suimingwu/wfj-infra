pipelineJob('ftms-official-platform-clues-integration-prod') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-official-platform-clues-integration.git')
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-official-platform-clues-integration-prod.jenkinsfile'))
            sandbox()
        }
    }
}