pipelineJob('deployService') {

	definition {
		cps {
			script(readFileFromWorkspace('pipelines/deployService.jenkinsfile'))
			sandbox()
		}
	}
}