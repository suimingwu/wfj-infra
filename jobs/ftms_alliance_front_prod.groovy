pipelineJob('ftms-alliance-front-prod') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-alliance-front.git')
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-alliance-front-prod.jenkinsfile'))
            sandbox()
        }
    }
}