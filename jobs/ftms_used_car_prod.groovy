pipelineJob('ftms-used-car-prod') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-used-car.git')
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-used-car-prod.jenkinsfile'))
            sandbox()
        }
    }
}