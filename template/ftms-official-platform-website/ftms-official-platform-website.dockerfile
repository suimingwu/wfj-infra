FROM 192.168.119.4:5000/node:16.8.0 as builder

COPY ./ /root/project

WORKDIR /root/project

RUN npm install -g pnpm \
    && pnpm install \
    && pnpm build

FROM 192.168.119.4:5000/nginx:1.20.2-alpine

COPY --from=builder /root/project/dist /usr/share/nginx/html
COPY --from=builder /root/project/nginx.conf.template /etc/nginx/conf.d

RUN ln -snf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && echo Asia/Shanghai > /etc/timezone
ADD ./infra-script/entryPoint.sh /entryPoint.sh
RUN chmod +x /entryPoint.sh

ENTRYPOINT ["/entryPoint.sh"]

EXPOSE 80

CMD cat /etc/nginx/conf.d/nginx.conf.template > /etc/nginx/conf.d/default.conf  \
	&& nginx -g 'daemon off;'