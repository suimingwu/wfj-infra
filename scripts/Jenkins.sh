#!/bin/bash
# 192.168.119.1
docker rm -f ftms-Jenkins-new

docker run -d \
    -e TZ=Asia/Shanghai \
    -e JENKINS_OPTS="--prefix=/jenkins" \
    --restart always \
    --name ftms-Jenkins-new \
    --privileged=true \
    -p 8081:8080 \
    -p 50001:50000 \
    -v /data/jenkins_new:/var/jenkins_home \
    -v /var/run/docker.sock:/var/run/docker.sock \
    -v $(which docker):/usr/bin/docker \
    jenkins/jenkins:lts