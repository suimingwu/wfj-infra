pipelineJob('ftms-alliance-prod') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-alliance.git')
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-alliance-prod.jenkinsfile'))
            sandbox()
        }
    }
}