def call() {
    dir("ftms-official-platform-infra") {
        git credentialsId: '92a1a45e-4397-4664-8095-50f9931d5ddc', changelog: false,
                url: 'git@github.com:tw-ftms-official-platform/ftms-official-platform-infra.git'
        dir("resources") {
            docker.image("registry.cn-hangzhou.aliyuncs.com/acs/kubectl:1.14.8").inside("--privileged -u root") {
                configFileProvider([configFile(fileId: "jenkins-k8s-config", variable: "K8S_CONFIG")]) {
                    sh "mkdir -p ~/.kube && cp $K8S_CONFIG ~/.kube/config"
                    sh "kubectl apply -f ./k8s/component/dev"
                    sh "kubectl apply -f ./k8s/component/testing"
                    sh "kubectl apply -f ./k8s/component/uat"
                }
            }
            docker.image("registry.cn-hangzhou.aliyuncs.com/acs/kubectl:1.14.8").inside("--privileged -u root") {
                configFileProvider([configFile(fileId: "k8s-prod-config", variable: "K8S_CONFIG")]) {
                    sh "mkdir -p ~/.kube && cp $K8S_CONFIG ~/.kube/config"
                    sh "kubectl apply -f ./k8s/component/prod/"
                }
            }
        }
    }
}