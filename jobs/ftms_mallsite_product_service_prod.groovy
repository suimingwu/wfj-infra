pipelineJob('ftms-mallsite-product-service-prod') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-mallsite-product-service.git')
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-mallsite-product-service-prod.jenkinsfile'))
            sandbox()
        }
    }
}