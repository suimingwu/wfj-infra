pipelineJob('ftms-official-service-prod') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-official-service.git')
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-official-service-prod.jenkinsfile'))
            sandbox()
        }
    }
}