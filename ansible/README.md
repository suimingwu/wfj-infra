        # ftms-official-config

配置仓库

## 依赖

ansible 2.9.25

## ansible-playbook

1. 初始化一组机器：`ansible-playbook -e "targets=worker-uat" -i inventory playbooks/host.yaml`
2. 更新测试环境nginx: `ansible-playbook -i inventory playbooks/testing-nginx.yaml`
3. 更新跳板机代理服务：`ansible-playbook -i inventory playbooks/haproxy.yaml`
4. k8s node 初始化设置：`ansible-playbook -e "targets=worker-uat" -i inventory playbooks/k8s_node_init.yaml`

## nodePort 端口占用情况

|端口| service                           |namespace|
|----|-----------------------------------|----|
30000 | kubernetes-dashboard-np           | kubernetes-dashboard
30001 | ftms-official-platform-website-np | ftms-testing
30002 | ftms-alliance-front-np            | ftms-testing
30003 | prometheus-k8s-np                 | monitoring
30004 | grafana-np                        | monitoring
30005 | ftms-testing-zookeeper-np         | ftms-testing
30006 | ftms-testing-showdoc-np           | ftms-testing
30007 | ftms-official-platform-website-np | ftms-uat
30008 | ftms-alliance-np                  | ftms-testing
30009 | ftms-alliance-front-np            | ftms-uat
30010 | ftms-testing-mysql-np             | ftms-testing
30011 | ftms-union-np                     | ftms-testing
30012 | ftms-skywalking-ui-np             | ftms-testing-apm
30013 | ftms-skywalking-oap-np            | ftms-testing-apm
30014 | ftms-uat-mysql-np                 | ftms-uat
30015 | ftms-alliance-np                  | ftms-uat
30016 | ftms-kibana-np                    | monitoring
30017 | ftms-official-service-np          | ftms-testing
30018 | ftms-apm-server-np                | monitoring
30019 | ftms-website-api-np               | ftms-testing
30020 | ftms-dev-mysql-np                 | ftms-dev
30021 | ftms-alliance-front-np            | ftms-dev
30022 | ftms-alliance-np                  | ftms-dev
30023 | ftms-official-platform-website-np | ftms-dev
30024 | ftms-official-service-np          | ftms-dev
30025 | ftms-union-np                     | ftms-dev
30026 | ftms-website-api-np               | ftms-dev
30027 | ftms-website-service-np           | ftms-testing
30028 | ftms-website-service-np           | ftms-dev
30029 | ftms-official-service-np          | ftms-uat
30030 | ftms-metabase                     | ftms-testing
30032 | ftms-zkui                         | ftms-testing
30033 | ftms-official-api-platform        | ftms-testing
30034 | ftms-wap                          | ftms-testing
30035 | ftms-union                        | ftms-uat
30036 | ftms-mallsite-app                 | ftms-dev
30037 | ftms-website-pc                   | ftms-dev
30038 | ftms-website-pc                   | ftms-testing
30039 | ftms-website-pc                   | ftms-uat
30040 | ftms-mallsite-app             | ftms-testing
30041 | ftms-mallsite-app             | ftms-uat
30042 | ftms-union-legacy             | ftms-uat
30043 | ftms-dev-mysql8            | ftms-dev
30044 | ftms-testing-mysql8            | ftms-testing
30045 | ftms-uat-mysql8            | ftms-uat
30046 | ftms-official-platform-clues-integration-np            | ftms-dev
30047 | ftms-official-platform-clues-integration-np            | ftms-testing
30048 | ftms-official-platform-clues-integration-np            | ftms-uat
30049 | ftms-union-cardcenter            | ftms-uat
30050 | .6生产库           | 

## haproxy 端口占用情况

|端口|暴露的服务|是否可以公网访问|
|----|----|----|
8001 | 开发库 | 是
8002 | 旧的jenkins | 是
8003 | nexus | 是
8004 | k8s apiserver | 是
30000 | k8s dashboard | 否
30003 | prometheus | 否
30004 | grafana | 否
30012 | skywalking | 否
30016 | kibana | 否
30030 | metabase | 否
30032 | ftms-zkui | 否
32567 | prodkube | 否
30049 | 生产从库 | 否
30051 | 智能平台静态资源预览 |否
30052 | 商城管理后台静态资源预览|否         | 