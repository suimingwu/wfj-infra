pipelineJob('ftms-mallsite-app') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-order.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-mallsite-app.jenkinsfile'))
            sandbox()
        }
    }
}