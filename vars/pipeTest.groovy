package vars

def call() {
    docker.image("192.168.119.4:5000/gradle:6.6.1-jdk11").inside("--privileged -u root") {
        sh "gradle clean check"
    }
}