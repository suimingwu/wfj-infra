pipelineJob('ftms-official-platform-clues-integration') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-official-platform-clues-integration.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-official-platform-clues-integration.jenkinsfile'))
            sandbox()
        }
    }
}