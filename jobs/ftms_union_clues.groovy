pipelineJob('ftms-union-clues') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-union.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-union-clues.jenkinsfile'))
            sandbox()
        }
    }
}