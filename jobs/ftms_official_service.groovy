pipelineJob('ftms-official-service') {

    properties {
        githubProjectUrl('git地址')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-official-service.jenkinsfile'))
            sandbox()
        }
    }
}