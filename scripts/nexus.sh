#!/bin/bash
#deploy on 192.168.119.5

mkdir -p /data/nexus-data && chown -R 200 /data/nexus-data

docker run -d \
    --restart always \
    -p 8081:8081 \
    --name ftms-nexus \
    -v /data/nexus-data:/nexus-data \
    sonatype/nexus3
