#!/bin/bash

#deploy 192.168.101.200
docker rm -f ftms-haproxy

cp -rf ../conf/haproxy/* /data/haproxy/conf

docker run -d \
    --restart always \
    --net host \
    --name ftms-haproxy \
    -v /data/haproxy/conf:/usr/local/etc/haproxy:ro \
    haproxy:2.3