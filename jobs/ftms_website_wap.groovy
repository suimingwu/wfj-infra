pipelineJob('ftms-website-wap') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-website-h5.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-website-wap.jenkinsfile'))
            sandbox()
        }
    }
}