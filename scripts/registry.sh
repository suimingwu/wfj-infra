#!/bin/bash
#deploy 192.168.119.4

docker run -d \
    -v /data/registry:/var/lib/registry \
    -p 5000:5000 \
    --restart=always \
    --privileged=true \
    --name ftms-registry \
    registry:latest

