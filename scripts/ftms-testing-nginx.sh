#!/bin/bash
# deploy on 192.168.119.1
docker rm -f ftms-testing-nginx
mkdir -p /data/nginx/conf
mkdir -p /data/nginx/certs
cp -rf ../conf/nginx-test/* /data/nginx/conf
docker run -d \
    --name=ftms-testing-nginx \
    --restart=always \
    -p 80:80 \
    -p 443:443 \
    -v /data/nginx/conf:/etc/nginx/conf.d \
    -v /data/nginx/certs:/etc/nginx/certs \
    nginx:1.20.2-alpine
