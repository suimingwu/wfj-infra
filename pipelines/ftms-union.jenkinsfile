@Library(('ftms-official-platform-jenkins-libs')) _
pipeline {
    agent any
    environment {
        DOCKER_REGISTRY = "192.168.119.4:5000"
        PROD_DOCKER_REGISTRY = "registry.ftms-k8s.com.cn/ftms-gfxt"
    }

    stages {
        stage("⚡ Checkout code") {
            steps {
                script {
                    pipeCheckScm("git@github.com:tw-ftms-official-platform/ftms-union.git", "*/master")
                    env.GITCOMMIT_ID = sh(returnStdout: true, script: 'git rev-parse --short HEAD').trim()
                    env.GIT_BRANCH = sh(returnStdout: true, script: 'git name-rev --name-only HEAD').trim()
                }
            }
        }

        stage("⛓ Build image and push") {
            steps {
                script {
                    def dockerfile = dockerfile()
                    writeFile file: 'Dockerfile', text: dockerfile
                    docker.withRegistry("http://${DOCKER_REGISTRY}") {
                        def image = docker.build("ftms-union:${GITCOMMIT_ID}")
                        image.push()
                        image.push("latest")
                    }
                }
            }
        }

        stage("⛩ Deploy app to dev") {
            steps{
                script {
                    env.DEPLOY_ENV = "dev"
                    pipeDeploy()
                }
            }
        }

        stage("⏳ Approval of deploy to testing") {

            steps {
                script {
                    try {
                        env.DEPLOY_TEST_FLAG = "true"
                        timeout(time: 5, unit: "MINUTES") {
                            input message: "Deploy ftms-union to test?"
                        }
                    } catch(err) {
                        env.DEPLOY_TEST_FLAG = "false"
                    }
                }
            }
        }

        stage("⛩ Deploy app to testing") {
            when {
                expression { DEPLOY_TEST_FLAG == "true" }
            }
            steps{
                script {
                    env.DEPLOY_ENV = "testing"
                    pipeDeploy()
                }
            }
        }

        stage("⏳ Approval of deploy to uat") {
            steps {
                script {
                    try {
                        env.DEPLOY_UAT_FLAG = "true"
                        timeout(time: 5, unit: "MINUTES") {
                            input message: "Deploy ftms-union to uat?"
                        }
                    } catch(err) {
                        env.DEPLOY_UAT_FLAG = "false"
                    }
                }
            }
        }

        stage("⛩ Deploy app to uat") {
            when {
                expression { DEPLOY_UAT_FLAG == "true" }
            }
            steps{
                script {
                    env.DEPLOY_ENV = "uat"
                    pipeDeploy()
                }
            }
        }
    }
    post {
        failure {
            script {
                notify.weComNotify("构建失败 ❌")
            }
        }
    }

}

def dockerfile() {
    return '''
FROM 192.168.119.4:5000/php:5.6.40-fpm

EXPOSE 80

RUN ln -snf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime && echo Asia/Shanghai > /etc/timezone
RUN    apt update \
    && apt install -y libfreetype6-dev libjpeg62-turbo-dev libpng-dev nginx \
    && docker-php-ext-install -j$(nproc) iconv \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-configure pdo_mysql \
    && docker-php-ext-install pdo_mysql \
    && pecl install redis-4.3.0 \
    && docker-php-ext-enable redis
COPY . /app/ftms-union/
WORKDIR /app/ftms-union/
RUN ls /app/ftms-union/|grep -v Config|xargs chmod 777 -R
CMD ["/bin/sh","-c","nginx -c /etc/nginx/nginx.conf && nginx -s reload && php-fpm"]

    '''
}