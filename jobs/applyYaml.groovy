pipelineJob('applyYaml') {

	properties {
		githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-official-platform-jenkins-libs.git')
		pipelineTriggers {
			triggers {
				githubPush()
			}
		}
	}

	definition {
		cps {
			script(readFileFromWorkspace('pipelines/applyYaml.jenkinsfile'))
			sandbox()
		}
	}
}