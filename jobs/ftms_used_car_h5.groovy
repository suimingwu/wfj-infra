pipelineJob('ftms-used-car-h5') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-used-car-h5.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-used-car-h5.jenkinsfile'))
            sandbox()
        }
    }
}