package vars
/* notify.groovy
    依赖以下Jenkins plugin:
    GitHub plugin
    HTTP Request Plugin
    build user vars plugin
*/

def getChangeLog() {
    def changeLog = ""
    def MAX_MSG_LEN = 200
    def changeLogSets = currentBuild.changeSets

    for (int i = 0; i < changeLogSets.size(); i++) {
        def entries = changeLogSets[i].items
        for (int j = 0; j < entries.length; j++) {
            def entry = entries[j]
            truncatedMsg = entry.msg.take(MAX_MSG_LEN)
            commitTime = new Date(entry.timestamp).format("yyyy-MM-dd HH:mm:ss")
            changeLog += " - ${truncatedMsg} [${entry.author} ${commitTime}]\n"
        }
    }

    if (!changeLog) {
        changeLog = " - No new changes"
    }

    return changeLog
}

def weComNotify(Status) {
    wrap([$class: 'BuildUser']) {
        def weComBotUrl = "https://qyapi.weixin.qq.com/cgi-bin/webhook/send?key=1cea43c2-dc0d-482b-950e-99f916336d35"
        def changeLog = getChangeLog()
        def buildUser = env.BUILD_USER

        if (buildUser == "SCM Change") {
            buildUser = sh (
                script: "git --no-pager show -s --format=%an/%ae",
                returnStdout: true
            ).trim()
        }

        def reqBody = """
            {
            "msgtype": "markdown",
            "markdown": {
                "content": "### 构建信息 \n> 应用名称: **<font color='info'>${env.JOB_BASE_NAME}</font>** \n> 构建结果: **${Status}** \n> 当前版本: **${env.GITCOMMIT_ID}**\n> 构建发起: **${buildUser}** \n> 构建日志: [点击查看详情](${env.BUILD_URL}console) \n\n#### 更新记录 \n${changeLog}"
            }
            }
        """

        httpRequest acceptType: 'APPLICATION_JSON_UTF8',
                consoleLogResponseBody: false,
                contentType: 'APPLICATION_JSON_UTF8',
                httpMode: 'POST',
                ignoreSslErrors: true,
                requestBody: reqBody,
                responseHandle: 'NONE',
                url: "${weComBotUrl}",
                quiet: true
    }
}

def setBuildStatus() {
    step([
        $class: "GitHubCommitStatusSetter",
        reposSource: [$class: "AnyDefinedRepositorySource"],
        contextSource: [$class: "DefaultCommitContextSource"],
        errorHandlers: [[$class: "ChangingBuildStatusErrorHandler", result: "UNSTABLE"]],
        commitShaSource: [$class: "BuildDataRevisionShaSource"],
        statusBackrefSource: [$class: "ManuallyEnteredBackrefSource", backref: "${BUILD_URL}console"],
        statusResultSource: [$class: "DefaultStatusResultSource"]
    ]);
}

return this
