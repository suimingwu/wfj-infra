pipelineJob('ftms-web-prod') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-web.git')
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-web-prod.jenkinsfile'))
            sandbox()
        }
    }
}