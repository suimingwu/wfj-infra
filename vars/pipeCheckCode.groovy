package vars

def call() {
    docker.image("192.168.119.4:5000/node:16.8.0").inside("--privileged -u root") {
        sh "npm install -g pnpm && pnpm install && pnpm code-check"
    }
}
