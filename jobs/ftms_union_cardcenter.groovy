pipelineJob('ftms-union-cardcenter') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-union-cardcenter.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-union-cardcenter.jenkinsfile'))
            sandbox()
        }
    }
}