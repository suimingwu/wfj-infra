pipelineJob('ftms-used-car-pc') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-used-car-pc.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-used-car-pc.jenkinsfile'))
            sandbox()
        }
    }
}