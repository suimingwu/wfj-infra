pipelineJob('ftms-official-website-manager-site') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-official-website-manager-site.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-official-website-manager-site.jenkinsfile'))
            sandbox()
        }
    }
}