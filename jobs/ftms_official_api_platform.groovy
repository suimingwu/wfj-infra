package jobs

pipelineJob('ftms-official-api-platform') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-official-api-platform.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-official-api-platform.jenkinsfile'))
            sandbox()
        }
    }
}