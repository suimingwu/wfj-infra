pipelineJob('ftms-website-service-prod') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-website-service.git')
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-website-service-prod.jenkinsfile'))
            sandbox()
        }
    }
}