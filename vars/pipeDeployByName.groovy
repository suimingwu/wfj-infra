package vars

def call(dockerfileName) {
    if (env.DEPLOY_ENV == "prod") {
        env.CONFIG = "k8s-prod-config"
        env.REGISTRY = "${PROD_DOCKER_REGISTRY}"
    }else {
        env.CONFIG = "jenkins-k8s-config"
        env.REGISTRY = "${DOCKER_REGISTRY}"
    }
    dir("ftms-official-platform-infra") {
        git credentialsId: '92a1a45e-4397-4664-8095-50f9931d5ddc', changelog: false,
            url: 'git@github.com:tw-ftms-official-platform/ftms-official-platform-infra.git'
        env.APPYAML = "${DEPLOY_ENV}/${dockerfileName}"
        dir("resources") {
            echo "render the k8s template"
            sh "./render.sh"
            docker.image("registry.cn-hangzhou.aliyuncs.com/acs/kubectl:1.14.8").inside("--privileged -u root") {
                configFileProvider([configFile(fileId: "${CONFIG}", variable: "K8S_CONFIG")]) {
                    sh "mkdir -p ~/.kube && cp $K8S_CONFIG ~/.kube/config"
                    sh "kubectl apply -f ./k8s/${APPYAML}-${DEPLOY_ENV}.yaml"
                }
            }
        }
    }
}