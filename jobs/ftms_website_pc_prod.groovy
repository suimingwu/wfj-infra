pipelineJob('ftms-website-pc-prod') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-website-pc.git')
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-website-pc-prod.jenkinsfile'))
            sandbox()
        }
    }
}