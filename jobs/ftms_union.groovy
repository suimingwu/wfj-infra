pipelineJob('ftms-union') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-union.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-union.jenkinsfile'))
            sandbox()
        }
    }
}