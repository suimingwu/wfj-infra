pipelineJob('ftms-official-website-manager-front') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-official-website-manager-front.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-official-website-manager-front.jenkinsfile'))
            sandbox()
        }
    }
}