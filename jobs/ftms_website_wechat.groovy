pipelineJob('ftms-website-wechat') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-website-wechat.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-website-wechat.jenkinsfile'))
            sandbox()
        }
    }
}