pipelineJob('ftms-mallsite-member-service') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-order.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-mallsite-member-service.jenkinsfile'))
            sandbox()
        }
    }
}