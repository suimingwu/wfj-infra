pipelineJob('ftms-union-prod') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-union.git')
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-union-prod.jenkinsfile'))
            sandbox()
        }
    }
}