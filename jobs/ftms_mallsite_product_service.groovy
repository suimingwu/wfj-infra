pipelineJob('ftms-mallsite-product-service') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-order.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-mallsite-product-service.jenkinsfile'))
            sandbox()
        }
    }
}