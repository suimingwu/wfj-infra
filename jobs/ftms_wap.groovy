pipelineJob('ftms-wap') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-wap.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-wap.jenkinsfile'))
            sandbox()
        }
    }
}