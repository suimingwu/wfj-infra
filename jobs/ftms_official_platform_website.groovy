pipelineJob('ftms-official-platform-website') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-official-platform-website.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-official-platform-website.jenkinsfile'))
            sandbox()
        }
    }
}