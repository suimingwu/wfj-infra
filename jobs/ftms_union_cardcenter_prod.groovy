pipelineJob('ftms-union-cardcenter-prod') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-union-cardcenter.git')
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-union-cardcenter-prod.jenkinsfile'))
            sandbox()
        }
    }
}