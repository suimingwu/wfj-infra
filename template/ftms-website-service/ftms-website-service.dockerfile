FROM 192.168.119.4:5000/fantito/jdk11-maven-git as builder

COPY . /data/ftms
WORKDIR /data/ftms

RUN unset MAVEN_CONFIG && ./mvnw clean package -U -Dmaven.test.skip=true

FROM 192.168.119.4:5000/openjdk11-maven:11.0.14.1

ENV TZ=Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone


COPY --from=builder /data/ftms/website_service/target/*.jar app.jar

RUN sh -c 'touch /app.jar'

CMD [ "sh", "-c", "java $JAVA_OPTS -jar  /app.jar" ]

EXPOSE 8080