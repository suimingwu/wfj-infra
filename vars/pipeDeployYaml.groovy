package vars

def call() {
    if (env.DEPLOY_ENV == "prod") {
        env.CONFIG = "k8s-prod-config"
    }else {
        env.CONFIG = "jenkins-k8s-config"
    }
    dir("ftms-official-platform-infra") {
        git credentialsId: '92a1a45e-4397-4664-8095-50f9931d5ddc', changelog: false,
                url: 'git@github.com:tw-ftms-official-platform/ftms-official-platform-infra.git'
        dir("resources") {
            docker.image("registry.cn-hangzhou.aliyuncs.com/acs/kubectl:1.14.8").inside("--privileged -u root") {
                configFileProvider([configFile(fileId: "${CONFIG}", variable: "K8S_CONFIG")]) {
                    sh "mkdir -p ~/.kube && cp $K8S_CONFIG ~/.kube/config"
                    sh "kubectl apply -f ./k8s/${DEPLOY_ENV}/${YAML}.yaml"
                }
            }
        }
    }
}