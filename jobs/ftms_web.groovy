pipelineJob('ftms-web') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-web.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-web.jenkinsfile'))
            sandbox()
        }
    }
}