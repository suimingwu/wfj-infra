pipelineJob('ftms-alliance-front') {

    properties {
        githubProjectUrl('https://github.com/tw-ftms-official-platform/ftms-alliance-front.git')
        pipelineTriggers {
            triggers {
                githubPush()
            }
        }
    }

    definition {
        cps {
            script(readFileFromWorkspace('pipelines/ftms-alliance-front.jenkinsfile'))
            sandbox()
        }
    }
}