FROM 192.168.119.4:5000/gradle:6.6.1-jdk11 as builder

COPY . /data/ftms
WORKDIR /data/ftms

RUN gradle build -x test --no-build-cache

FROM 192.168.119.4:5000/adoptopenjdk/openjdk11:alpine-jre

RUN apk add --no-cache tzdata
ENV TZ=Asia/Shanghai
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

COPY --from=builder /data/ftms/build/libs/*.jar app.jar

RUN sh -c 'touch /app.jar'

ENTRYPOINT [ "sh", "-c", "java $JAVA_OPTS -jar /app.jar" ]

EXPOSE 8080
